let input = document.getElementById("addTask");
let todoTable = document.getElementById("todoTable");
let doneTable = document.getElementById("doneTable");
let memoTable = document.getElementById("memoTable");

document.getElementById("addMemo").addEventListener("click", function() {
  if(input.value !== "") {
    let memoItemChild = document.createElement("TD");
    let memoItemChildText = document.createTextNode(input.value);
    memoItemChild.appendChild(memoItemChildText);
    document.getElementById("addTask").value = "";
    let memoItem = document.createElement("TR");
    memoItem.appendChild(memoItemChild);

    memoTable.appendChild(memoItem);

    memoItem.addEventListener("click", function() {
      
      memoItem.parentNode.removeChild(memoItem)
    })
  }
})
function todo() {
  document.getElementById("doneTable").className = "invisible";
  document.getElementById("todoTable").className = "visible";

}
function done() {
  document.getElementById("todoTable").className = "invisible";
  document.getElementById("doneTable").className = "visible";

}
function visible() {
  document.getElementById("todoTable").className = "visible";
  document.getElementById("doneTable").className = "visible";
}
document.getElementById("addBtn").addEventListener("click", function() {
  if(input.value !== "") {
    let itemChild = document.createElement("TD");
    let itemChildText = document.createTextNode(input.value);
    itemChild.appendChild(itemChildText);
    document.getElementById("addTask").value = "";

    let item = document.createElement("TR");
    item.appendChild(itemChild);

    todoTable.appendChild(item);

    item.addEventListener("click", function() {
      
      let doneItemChild = document.createElement("TD");
    let doneItemChildText = document.createTextNode(item.childNodes[0].innerHTML);
    doneItemChild.appendChild(doneItemChildText);

    let doneItem = document.createElement("TR");
    doneItem.appendChild(doneItemChild);

    doneItem.addEventListener("click", function() {
      
      doneItem.parentNode.removeChild(doneItem)
    })

    doneTable.appendChild(doneItem);
    item.parentNode.removeChild(item);   
    })
  }
  
});




















